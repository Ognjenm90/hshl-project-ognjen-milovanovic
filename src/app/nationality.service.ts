import { Injectable } from "@angular/core";
import { Nationality } from './nationality'
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpErrorResponse, HttpHeaders, } from "@angular/common/http";
import { Observable , of} from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
@Injectable()
export class NationalityService {
    private readonly API_URL = 'api/nationalities';
    
    dataChange: BehaviorSubject<Nationality[]> = new BehaviorSubject<Nationality[]>([]);


 

     
  
  private heroesUrl = 'api/nationalities';  // URL to web api
     


    constructor(private httpClient: HttpClient) { }

   
  

    getNationality (): Observable<Nationality[]> {
        return this.httpClient.get<Nationality[]>(this.API_URL);
          
     
      }

  public addNationality(nationality: Nationality): void {
       this.httpClient.post(this.API_URL, nationality).subscribe();
    }


    public updateNationality(nacionalnost: Nationality): void {

        this.httpClient.put(this.API_URL, nacionalnost).subscribe();
    }
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
    
          // TODO: send the error to remote logging infrastructure
          console.error(error); // log to console instead
    
          // TODO: better job of transforming error for user consumption
       
    
          // Let the app keep running by returning an empty result.
          return of(result as T);
        };
      }
    public deleteNationality(id: number): void {
        this.httpClient.delete(this.API_URL + id).subscribe();
    }  
}